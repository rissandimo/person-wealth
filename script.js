//DOM ELEMENTS
const addUserButton = document.getElementById('add-user');
const doubleMoneyButton = document.getElementById('double-money');
const showMillionaresButton = document.getElementById('show-millionares');
const sortButtonRichest = document.getElementById('sort');
const sortButtonPoorest = document.getElementById('sort2');
const calculateWealthButton = document.getElementById('calculate-Wealth');
const mainArea = document.getElementById('main');
const personList = document.getElementById('person-list');

let people = [];

function addNewUser(newUser){
    people.push(newUser);
    renderPeople();
}

function calculateAllIncomes(){
    const totalWealth = people.reduce( (accumulator, currentPerson) => (accumulator + currentPerson.netWorth), 0);
    const domElement = document.createElement('div');
    domElement.innerHTML = `<h3>The total wealth of everyone combined is ${formatMoney(totalWealth)}</h3>`;
    mainArea.appendChild(domElement);
}

function sortByAscending(){
    people.sort(function(personA, personB){
       return personA.netWorth - personB.netWorth; 
    })
    renderPeople();
}

function sortByDescending(){
    people.sort(function (personA, personB) {
        return personB.netWorth - personA.netWorth;
    });
    renderPeople();
}

function sortByMillionares(){
   const millionares = people.filter(person => person.netWorth >= 1000000);
    renderPeople(millionares);
}

function doubleMoney(){
    people.forEach(person => person.netWorth *= 2 );
    renderPeople();
}

function formatMoney(amount){
    return '$' + amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}; 

//fetch random user and set money
async function getRandomUser(){
   const response = await fetch('https://randomuser.me/api');
   const data = await response.json();

   const randomuser = data.results[0];
   
   const person = {
        name: `${randomuser.name.first} ${randomuser.name.last}`,
        netWorth: Math.floor(Math.random() * 1000000),
   }

   addNewUser(person); 
   
}

function renderPeople(personArray = people) {

    mainArea.innerHTML = '<h2><strong>Person</strong>  Wealth</h2>';
    
    personArray.forEach( person => {

        const personDiv = document.createElement('div');
        personDiv.classList.add('person');

        personDiv.innerHTML = `<strong>${person.name}</strong> ${formatMoney(person.netWorth)}`;

        mainArea.appendChild(personDiv);
    });
    
}

getRandomUser();
getRandomUser();
getRandomUser();
//EVENT LISTENERS

addUserButton.addEventListener('click', getRandomUser);
doubleMoneyButton.addEventListener('click', doubleMoney);
sortButtonRichest.addEventListener('click', sortByDescending);
sortButtonPoorest.addEventListener('click', sortByAscending);
showMillionaresButton.addEventListener('click', sortByMillionares);
calculateWealthButton.addEventListener('click', calculateAllIncomes);
